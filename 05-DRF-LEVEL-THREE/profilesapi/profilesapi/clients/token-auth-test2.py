import requests


def client():
    # token_h = "Token f4d04131d2a6e7b90c86b0d05963d9ae8c02067e"

    # data = {
    #     "username": "resttest",
    #     "email": "test@rest.com",
    #     "password1": "Django-0117",
    #     "password2": "Django-0117"
    #     }

    # response = requests.post("http://localhost:8000/api/rest-auth/registration/", data=data)

    token_h = "Token a79ca2327a7065e0fbc346f15892a9b4f8acd6b6"

    headers = {"Authorization": token_h}

    response = requests.get("http://localhost:8000/api/profiles/", headers=headers)

    print("Status Code: ", response.status_code)
    response_data = response.json()
    print(response_data)


if __name__ == "__main__":
    client()