# Generated by Django 2.2.1 on 2019-05-04 18:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='article',
            old_name='publication_Date',
            new_name='publication_date',
        ),
    ]
