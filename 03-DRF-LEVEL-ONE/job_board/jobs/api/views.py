from rest_framework import status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from jobs.models import JobOffer
from jobs.api.serializers import JobOfferSerializer


# @api_view(["GET", "POST"])
# def JobOffer_list_create_api_view(request):

#     if request.method == "GET":
#         JobOffers = JobOffer.objects.filter(active=True)
#         serializer = JobOfferSerializer(JobOffers, many=True)
#         return Response(serializer.data)

#     elif request.method == "POST":
#         serializer = JobOfferSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, 
#             status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# @api_view(["GET", "PUT", "DELETE"])
# def JobOffer_detail_api_view(request, pk):
#     try:
#         JobOffer = JobOffer.objects.get(pk=pk)
#     except JobOffer.DoesNotExist:
#         return Response({"error": {
#             "code": 404,
#             "message": "JobOffer not found!"
#         }}, status=status.HTTP_404_NOT_FOUND)

#     if request.method == "GET":
#         serializer = JobOfferSerializer(JobOffer)
#         return Response(serializer.data)

#     elif request.method == "PUT":
#         serializer = JobOfferSerializer(JobOffer, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#     elif request.method == "DELETE":
#         JobOffer.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)

class JobOfferListCreateAPIView(APIView):

    def get(self, request):
        jobs = JobOffer.objects.filter(available=True)
        serializer = JobOfferSerializer(jobs, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = JobOfferSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, 
            status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class JobOfferDetailAPIView(APIView):

    def get_object(self, pk):
        job = get_object_or_404(JobOffer, pk=pk)
        return job

    def get(self, request, pk):
        job = self.get_object(pk)
        serializer = JobOfferSerializer(job)
        return Response(serializer.data)

    def put(self, request, pk):
        job = self.get_object(pk)
        serializer = JobOfferSerializer(job, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        job = self.get_object(pk)
        job.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


# class JournalistListCreateAPIView(APIView):

#     def get(self, request):
#         Journalists = Journalist.objects.all()
#         serializer = JournalistSerializer(Journalists, 
#                                           many=True,
#                                           context = {'request': request})
#         return Response(serializer.data)

#     def post(self, request):
#         serializer = JournalistSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, 
#             status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)