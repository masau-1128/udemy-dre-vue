// // var app = new Vue({
// //     el: '#app',
// //     data: {
// //         message: "Hello World!",
// //         value: 5,
// //         imgSrc: "https://www.ippome.com/static/img/logo.png",
// //         link: "https://vuejs.org"
// //     }
// // })

// // var app = new Vue({
// //     el: '#app',
// //     data: {
// //         lesson: "Events and Methods",
// //         counter: 0
// //     },
// //     methods: {
// //         incrementCounter() {
// //             this.counter += 1;
// //             console.log(this.counter);
// //             if (this.counter === 10) {
// //                 alert("Counter is at 10!");
// //             }
// //         },
// //         overTheBox() {
// //             console.log("Over The Green Box!");
// //         }
// //     }
// // });


// // var app = new Vue({
// //     el: "#app",
// //     data: {
// //         // auth: false,
// //         product: "sunglasses",
// //         quantity: 150,
// //         sale: true
// //     }
// // })


// // var app = new Vue({
// //     el: "#app",
// //     data: {
// //         flag: true,
// //         styleObject: {
// //             backgroundColor: 'green', 
// //             border: '5px solid orange' 
// //         }
// //     },
// //     methods: {
// //         changeShape() {
// //             this.flag = !this.flag;
// //         }
// //     }
// // })


// // var app = new Vue({
// //     el: "#app",
// //     data: {
// //         // users: ["alice", "bob", "batman", "robin", "superman"]
// //         users: [{
// //             id: 567,
// //             name: "alice",
// //             profession: "developer"
// //         },
// //         {
// //             id: 568,
// //             name: "alice",
// //             profession: "developer"
// //         },
// //         {
// //             id: 569,
// //             name: "batman",
// //             profession: "manager"
// //         },
// //         {
// //             id: 570,
// //             name: "robin",
// //             profession: "designer"
// //         },
// //         {
// //             id: 571,
// //             name: "superman",
// //             profession: "developer"
// //         }
// //     ]
// //     }
// // });


// // var app = new Vue({
// //     el: "#app",
// //     data: {
// //         first_name: "John",
// //         last_name: "Doe"
// //     },
// //     computed: {
// //         getRandomComputed() {
// //             return Math.random();
// //     },
// //     fullName() {
// //         return `${ this.first_name } ${this.last_name}`;
// //     }
// // },
// //     methods: {
// //         getRandomNumber() {
// //             return Math.random();
// //         }
// //     }
// // });

// // var app = new Vue({
// //     el:"#app",
// //     data: {
// //         // text: '',
// //         // checked: true,
// //         // city: ''
// //         // color: 'green'
// //         comment: null,
// //         comments: [],
// //         errors: null
// //     },
// //     methods: {
// //         onSubmit() {
// //             if (this.comment) {
// //             let new_comment = this.comment;
// //             this.comments.push(new_comment);
// //             this.comment = null;

// //             if (this.errors) {
// //                 this.errors = null;
// //             }
// //         } else {
// //             this.errors = "The comment field can'gt be empty";
// //         }
// //     }
// //     }
// // });


// comment list component
Vue.component("comment-list", {
    props: {
        comments: {
            type: Array,
            required: true
            // default: true
        }
    },
    data: function() {
        return {
            new_comment: null,
            comment_author: null,
            error: null
        }
    },
    methods: {
        submitComment() {
            if (this.new_comment && this.comment_author) {
                this.$emit('submit-comment', { username: this.comment_author, content: this.new_comment});
                this.new_comment = null;
                this.comment_author = null;

                if (this.error) {
                    this.error = null;
                }
            } else {
                this.error = "please fill out both fields!";
            }
            // this.$emit('submit-comment', {username: this.comment_author, content: this.new_comment })
        }
    },
    template: `
        <div class="mt-2">

            <div class="container">

            <single-comment v-for="(comment, index) in comments" :comment="comment" :key="index"></single-comment>

            <hr>

            <h3>{{ error }}</h3>

            <form @submit.prevent="submitComment" class="mb-3">
                <div class="form-group">
                <label for="commentAuthor">Your Username</label>
                    <input  class="form-control" id="commentAuthor" type="text" v-model="comment_author">
                </div>

                <div class="form-group">
                <label for="commentText">Add a comment</label>
                    <textarea class="form-control" id="commentText" rows="3" cols="40" v-model="new_comment">
                    </textarea>
                </div>

                <button type="submit" class="btn btn-sm btn-primary">Publish</button>

            </form>
            </div>
        </div>
    `
})


// single comment component
Vue.component("single-comment", {
    props: {
        comment: {
            type: Object,
            required: true
            // default: true
        }
    },
    template: `
        <div class="mb-2">
        <div class="card">
        <div class="card-header">
            <p>Publish by: {{ comment.username }}</p>
        </div>
            <div class="card-body">
                <p>{{ comment.username }}</p>
                <p>{{ comment.content }}</p>
            </div>
            </div>
        </div>
    `
})


var app = new Vue({
    el: "#app",
    data: {
        comments: [
            {username: "alice", content: "first comment!"},
            {username: "bob", content: "hello world!"},
            {username: "ironman", content: "new armor coming soon!"},
            {username: "superman", content: "kryptonite is bad!"}
        ]
    },
    methods: {
        addNewComment(new_comment) {
            this.comments.push(new_comment);
        }
    }
});

Vue.config.devtools = true;

